wuzzuf-microservices
====================
A task for python developer from `wuzzuf`_,
consists of creating three microservices.

**Note**: Original Idea taken from `umermansoor/microservices`_ project.

Install Dependencies
--------------------

.. code-block:: bash

    $ pip install -r requirements.txt


Start Services
--------------

.. code:: bash

    # Export twitter keys.
    $ export TWITTER_CONSUMER_KEY='<TWITTER_CONSUMER_KEY>'
    $ export TWITTER_CONSUMER_SECRET='<TWITTER_CONSUMER_SECRET>'
    $ export TWITTER_ACCESS_TOKEN='<TWITTER_ACCESS_TOKEN>'
    $ export TWITTER_ACCESS_TOKEN_SECRET='<TWITTER_ACCESS_TOKEN_SECRET>'

    # Export watson project credentials.
    $ export WATSON_USERNAME='<TONE_ANALYZER_PROJECT_USERNAME>'
    $ export WATSON_PASSWORD='<TONE_ANALYZER_PROJECT_PASSWORD>'

    # Run services
    $ ./manage.sh start

Stop Services
-------------

.. code:: bash

    $ ./manage.sh stop

**Note**: Don't forget to give manage.sh execution permission.

Available Endpoints
-------------------

::

    # statistics
    http://127.0.0.1:5000/aggregate/<account_id>

    # tones
    http://127.0.0.1:5001/tones/<account_id>


.. _wuzzuf: https://wuzzuf.net
.. _umermansoor/microservices: https://github.com/umermansoor/microservices