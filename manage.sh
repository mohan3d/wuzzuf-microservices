#!/usr/bin/env bash

function start {
    python "services/twitter_aggregator.py" &
    python "services/tone_analyzer.py" &
}

function stop {
    ps -ef | grep "services/twitter_aggregator.py" | grep -v grep | awk '{print $2}' | xargs kill
	ps -ef | grep "services/tone_analyzer.py" | grep -v grep | awk '{print $2}' | xargs kill
}

if [ "$1" = "start" ]; then
    start
fi

if [ "$1" = "stop" ]; then
    stop
fi
