import calendar
import operator
from collections import defaultdict
from datetime import datetime


def _24_to_12(hour):
    s = datetime.strptime("{}".format(hour), "%H")
    return s.strftime("%I %p")


def account_statistics(account_api):
    """Calculates statistics of a twitter account,
    after consuming twitter api.

    Args:
        account_api (TwitterAPI): authenticated TwitterAPI client.

    Returns:
        dict: statistics of twitter account (tweets count, retweets count, ...).

    """
    days = defaultdict(int)
    hours = defaultdict(int)
    tweets_count = 0
    retweets_count = 0

    for tweet in account_api.timeline():
        days[tweet.weekday] += 1
        hours[tweet.hour] += 1

        if tweet.is_retweet:
            retweets_count += 1
        else:
            tweets_count += 1

    most_frequent_day = max(days.items(), key=operator.itemgetter(1))[0]
    most_frequent_hour = max(hours.items(), key=operator.itemgetter(1))[0]

    return {
        'number_of_tweets': tweets_count,
        'number_of_followers': account_api.followers_count(),
        'number_of_followings': account_api.followings_count(),
        'number_of_retweets': retweets_count,
        'peak_hour_of_daily_tweeting': '{} - {}'.format(
            _24_to_12(most_frequent_hour),
            _24_to_12(most_frequent_hour + 1)
        ),
        'peak_day_of_weekly_tweeting': calendar.day_name[most_frequent_day],
    }


def tone_analyze(account_api, watson_api):
    """Analyzes all tweets consumed from account_api timeline,
    through watson_api client.

    Args:
         account_api (TwitterAPI): authenticated TwitterAPI client.
         watson_api (WatsonAPI): authenticated WatsonAPI client.

    Returns:
        dict: a dictionary consists of tone id as a key and normalized score as a value.
    """
    scores = defaultdict(int)
    counts = defaultdict(int)

    for tweet in account_api.timeline():
        tones = watson_api.tone(tweet.content)

        for tone in tones:
            tone_id = tone['tone_id']
            tone_score = tone['score']

            scores[tone_id] += tone_score
            counts[tone_id] += 1

    return {
        tone_id: score / counts[tone_id] for tone_id, score in scores.items()
    }
