import tweepy
from watson_developer_cloud import ToneAnalyzerV3
import json


class TwitterAPI:
    """TwitterAPI methods must be implemented to be used in statistics/analysis"""

    def __init__(self, consumer_key, consumer_secret,
                 access_token, access_token_secret, account_id):
        pass

    def account_name(self):
        raise NotImplementedError

    def followers_count(self):
        raise NotImplementedError

    def followings_count(self):
        raise NotImplementedError

    def timeline(self):
        raise NotImplementedError


class Tweet:
    """Tweet consists of required fields for statistics/analysis"""

    def __init__(self, text, created_at, is_retweet):
        self._text = text
        self._created_at = created_at
        self._is_retweet = is_retweet

    @property
    def content(self):
        return self._text

    @property
    def weekday(self):
        return self._created_at.weekday()

    @property
    def hour(self):
        return self._created_at.hour

    @property
    def is_retweet(self):
        """Returns True if it is a retweet otherwise False"""
        return self._is_retweet

    @classmethod
    def from_tweepy_status(cls, status):
        """Returns an instance of Tweet filled with text, creation date and is_retweet"""
        return cls(
            text=status.text,
            created_at=status.created_at,
            is_retweet=hasattr(status, 'retweeted_status')
        )


class TweepyTwitterAPI(TwitterAPI):
    """Tweepy implementation of TwitterAPI"""

    def __init__(self, consumer_key, consumer_secret,
                 access_token, access_token_secret, user_id):
        super().__init__(consumer_key, consumer_secret,
                         access_token, access_token_secret, user_id)

        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        api = tweepy.API(auth)

        self._user = api.get_user(user_id)

    def account_name(self):
        """Returns display name of the authenticating account"""
        return self._user.name

    def followers_count(self):
        """Returns count of followers of the authenticating account"""
        return self._user.followers_count

    def followings_count(self):
        """Returns count of accounts followed by the authenticating account"""
        return self._user.friends_count

    def timeline(self, count=10 ** 4):
        """Returns list of Tweet objects of the authenticating account timeline"""
        return iter([
            Tweet.from_tweepy_status(status)
            for status in self._user.timeline(count=count)
        ])


class WatsonAPI:
    """Tiny wrapper around ToneAnalyzerV3"""
    def __init__(self, username, password, version='2017-09-21'):
        self._api = ToneAnalyzerV3(
            version=version,
            username=username,
            password=password
        )

    def tone(self, text, content_type='application/json'):
        """Returns tones list after analyzing text"""
        resp = self._api.tone({"text": text}, content_type)
        tones = resp['document_tone']['tones']
        return tones
