import os

from config import (ENV_ACCESS_TOKEN, ENV_ACCESS_TOKEN_SECRET,
                    ENV_CONSUMER_KEY, ENV_CONSUMER_SECRET)
from flask import Flask, jsonify
from twitter import account_statistics, TweepyTwitterAPI

app = Flask(__name__)


def get_account_stats(account_id):
    try:
        consumer_key = os.environ.get(ENV_CONSUMER_KEY)
        consumer_secret = os.environ.get(ENV_CONSUMER_SECRET)
        access_token = os.environ.get(ENV_ACCESS_TOKEN)
        access_token_secret = os.environ.get(ENV_ACCESS_TOKEN_SECRET)

        api = TweepyTwitterAPI(
            consumer_key,
            consumer_secret,
            access_token,
            access_token_secret,
            account_id
        )

        return account_statistics(api)
    except Exception as e:
        return str(e)


@app.route("/aggregate/<account_id>", methods=['GET'])
def aggregate(account_id):
    return jsonify(get_account_stats(account_id))


if __name__ == "__main__":
    app.run(port=5000, debug=True)
