import os

from config import (ENV_ACCESS_TOKEN, ENV_ACCESS_TOKEN_SECRET,
                    ENV_CONSUMER_KEY, ENV_CONSUMER_SECRET,
                    ENV_USERNAME, ENV_PASSWORD)
from flask import Flask, jsonify
from twitter import WatsonAPI, tone_analyze, TweepyTwitterAPI

app = Flask(__name__)


def get_account_tones(account_id):
    try:
        consumer_key = os.environ.get(ENV_CONSUMER_KEY)
        consumer_secret = os.environ.get(ENV_CONSUMER_SECRET)
        access_token = os.environ.get(ENV_ACCESS_TOKEN)
        access_token_secret = os.environ.get(ENV_ACCESS_TOKEN_SECRET)
        username = os.environ.get(ENV_USERNAME)
        password = os.environ.get(ENV_PASSWORD)

        account_api = TweepyTwitterAPI(
            consumer_key,
            consumer_secret,
            access_token,
            access_token_secret,
            account_id
        )

        watson_api = WatsonAPI(
            username,
            password
        )

        return tone_analyze(account_api, watson_api)
    except Exception as e:
        return str(e)


@app.route("/tones/<account_id>", methods=['GET'])
def aggregate(account_id):
    return jsonify(get_account_tones(account_id))


if __name__ == "__main__":
    app.run(port=5001, debug=True)
