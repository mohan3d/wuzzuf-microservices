import requests


class TwitterIndexer:
    """Tiny client based on requests, makes it easy to index tweet info"""

    def __init__(self, base_url):
        self._url = base_url

    def get_url(self, account_id):
        return "{}/{}/_update".format(self._url, account_id)

    def put(self, account_id, data):
        url = self.get_url(account_id)
        resp = requests.post(url, json={
            "doc": data,
            "doc_as_upsert": True
        })

        return resp.json()

    def index_name(self, account_id, name):
        """Indexes account name for given accountID

        Args:
            account_id (str): twitter accountID.
            name (str): twitter account name.

        Returns:
            dict: result of index_name query.
        """
        return self.put(account_id, dict(name=name))

    def index_statistics(self, account_id, stats):
        """Indexes account statistics for given accountID

        Args:
            account_id (str): twitter accountID.
            stats (dict): twitter account statistics.

        Returns:
            dict: result of index_statistics query.
        """
        return self.put(account_id, dict(statistics=stats))

    def index_tones(self, account_id, tones):
        """Indexes account tone-analysis for given accountID

        Args:
            account_id (str): twitter accountID.
            tones (dict): twitter account tone analysis.

        Returns:
            dict: result of index_tones query.
        """
        return self.put(account_id, dict(tones=tones))

    def index_tweets(self, account_id, tweets):
        """Indexes account tweets for given accountID

        Args:
            account_id (str): twitter accountID.
            tweets (list): list of string(tweets content)

        Returns:
            dict: result of index_tweets query.
        """
        return self.put(account_id, dict(tweets=tweets))
